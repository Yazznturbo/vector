#include <iostream>
#include "math/math.hpp"

constexpr auto v1 = math::vector<float, 3>(3.f, -2.f, 0.f);
constexpr math::vec3f v2{ 1.f, 7.f, 5.f};

constexpr auto v3 = v1 / v2;
constexpr bool dot_v1_v2 = (math::dot(v1, v2) == v1.dot(v2));
constexpr auto cross = math::cross(v1, v2);

////
////constexpr auto v3 = v1 * v2;
////constexpr auto v4 = v1 * 5.f;
//
//constexpr auto v3c0 = v3.data[0];
//constexpr auto v3c1 = v3.data[1];
//
//constexpr auto v4c0 = v4.data[0];
//constexpr auto v4c1 = v4.data[1];
//
//constexpr auto m = v2.min();
//constexpr auto s = v2.sum();
//constexpr auto p = v2.product();
//constexpr auto d = v2.dot();
//constexpr auto dd = v2.dot(v3);
//
//static_assert(v3c0 == 8.f, "");
//static_assert(v3c1 == 8.f, "");
//
//static_assert(v4c0 == 10.f, "");
//static_assert(v4c1 == 10.f, "");

int main() {
  math::vec3f x1{ 2.f, 3.f, 4.f };
  math::vec3f x2{ 2.f, 3.f, 4.f };

  auto xasdas = x1.normalized().cross(x2);

  math::vec3f::vector(1.f);

  x1.distance(v1);

  auto x3 = x1.x;

  // x3 = x1 * math::vector<float, 3>{1.f,1.f,1.f};

 // printf("%f", x3.x);

  std::cin.get();
}