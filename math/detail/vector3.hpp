#pragma once

#include "vector2.hpp"

namespace math {

template <typename T>
struct vector_base<T, 3>
{
  union
  { std::array<T, 3> data; // active member

    struct { T x, y, z; };
    vector2<T> xy;
  };
};

template <typename T>
class vector3
  : public vector<T, 3, vector3<T>>
{
public:
  using base = vector<T, 3, vector3<T>>;
  using base::base;

  constexpr vector3() noexcept = default;
	constexpr vector3(T x, T y, T z)
    : base { x, y, z }
  { }
  explicit constexpr vector3(vector2<T> const &xy, T z = T(0))
    : base{ xy[0], xy[1], z }
  { }

  template <typename Derived>
	vector3 constexpr cross(vector<T, 3, Derived> const &other) const {
		return math::cross(*this, other);
	}
};

template <typename T, typename Derived1, typename Derived2>
auto constexpr cross(vector<T, 3, Derived1> const &v1, vector<T, 3, Derived2> const &v2) {
  return std::conditional_t<!std::is_same_v<Derived1, void>, Derived1, std::conditional_t<!std::is_same_v<Derived2, void>, Derived2, vector<T, 3, void>>> { // Prefer specialized vector
    v1[1] * v2[2] - v1[2] * v2[1], v1[2] * v2[0] - v1[0] * v2[2], v1[0] * v2[1] - v1[1] * v2[0]
  };
}

using vec3i = vector3<int>;
using vec3f = vector3<float>;
using vec3d = vector3<double>;

} // namespace math