#pragma once

namespace detail {

template<class InputIt, class T, class BinaryOperation>
constexpr T accumulate(InputIt first, InputIt last, T init,
  BinaryOperation op) noexcept
{
  for (; first != last; ++first) {
    init = op(std::move(init), *first); // std::move since C++20
  }
  return init;
}

template<class InputIt1, class InputIt2, class T>
constexpr T inner_product(InputIt1 first1, InputIt1 last1,
  InputIt2 first2, T init) noexcept
{
  while (first1 != last1) {
    init = std::move(init) + *first1 * *first2; // std::move since C++20
    ++first1;
    ++first2;
  }
  return init;
}

//template <typename T>
//T constexpr sqrt(T x)
//{
//  if (x < T(0) || x >= math::inf<T>)
//    return math::nan<T>;
//
//  long double prev = 0.0L;
//  long double cur = static_cast<long double>(x);
//
//  while (cur != prev) {
//    prev = cur;
//    cur = 0.5L * (cur + x / cur);
//  }
//
//  return static_cast<T>(cur);
//}

} // detail