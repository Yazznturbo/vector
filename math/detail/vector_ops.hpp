#pragma once

namespace math::detail {

struct unknown_tag {};
struct scalar_tag {};
struct vector_tag {};
struct vector_spec_tag {};

template <class, class = std::void_t<>>
struct is_vector
  : std::false_type {};

template <class T>
struct is_vector<T, std::void_t<typename std::decay_t<T>::vector_tag_type>>
  : std::true_type {};

template <typename ...Ts>
constexpr bool any_vector = std::disjunction_v<is_vector<Ts>...>;

template <typename T>
constexpr auto tag_type()
{
  using U = std::decay_t<T>;

  if constexpr (is_vector<U>::value) {
    if constexpr (std::is_same_v<math::vector<U::value_type, U::size>, U>)
      return vector_tag{};
    else
      return vector_spec_tag{};
  }  else {
    if constexpr (std::is_arithmetic_v<U>)
      return scalar_tag{};
    else
      return unknown_tag{};
  }
}

template <typename T>
using tag_type_t = decltype(tag_type<T>());

template<class Tag, class T, class U>
constexpr decltype(auto) pick_by_tag(T&& a, U&& b)
{
  if constexpr(std::is_same_v<tag_type_t<T>, Tag>)
    return std::forward<T>(a);
  else
    return std::forward<U>(b);
}

template <typename Tag1, typename Tag2>
struct arithmetic_op;

template <>
struct arithmetic_op<vector_tag, scalar_tag> {
  template <typename LhsT, typename RhsT, typename Op>
  constexpr static decltype(auto) apply(LhsT &&lhs, RhsT &&rhs, Op op) {
    for (auto& val : lhs.data)
      val = op(val, rhs);
    return std::forward<LhsT>(lhs);
  }
};

template<>
struct arithmetic_op<scalar_tag, vector_tag> {
  template <typename LhsT, typename RhsT, typename Op>
  constexpr static decltype(auto) apply(LhsT &&lhs, RhsT &&rhs, Op op) {
    for (auto& val : rhs.data)
      val = op(lhs, val);
    return std::forward<RhsT>(rhs);
  }
};

template <>
struct arithmetic_op<vector_tag, vector_tag> {
  template <typename LhsT, typename RhsT, typename Op>
  constexpr static auto apply(LhsT &&lhs, RhsT &&rhs, Op op) {
    decltype(auto) result = pick_by_tag<vector_spec_tag>(lhs, rhs);

    for (std::size_t i = 0; i < lhs.data.size(); ++i)
      result[i] = op(lhs[i], rhs[i]);

    return result;
  }
};

template <> struct arithmetic_op<vector_tag, vector_spec_tag> : arithmetic_op<vector_tag, vector_tag> {};
template <> struct arithmetic_op<vector_spec_tag, vector_tag> : arithmetic_op<vector_tag, vector_tag> {};
template <> struct arithmetic_op<vector_spec_tag, scalar_tag> : arithmetic_op<vector_tag, scalar_tag> {};
template <> struct arithmetic_op<scalar_tag, vector_spec_tag> : arithmetic_op<scalar_tag, vector_tag> {};
template <> struct arithmetic_op<vector_spec_tag, vector_spec_tag> : arithmetic_op<vector_tag, vector_tag> {};

template<class T, class U, class Op>
inline decltype(auto) constexpr apply_arithmetic_op(T&& a, U&& b, Op op) {
  return arithmetic_op<tag_type_t<T>, tag_type_t<U>>::apply(std::forward<T>(a), std::forward<U>(b), op);
}

} // math::detail

template <typename VecT, typename std::enable_if_t<math::detail::is_vector<VecT>::value>* = nullptr>
auto constexpr operator-(VecT vec) noexcept { return math::detail::apply_arithmetic_op(VecT::value_type(-1), vec, std::multiplies<>{}); }

template <typename LhsT, typename RhsT, typename std::enable_if_t<math::detail::any_vector<LhsT, RhsT>>* = nullptr>
auto constexpr operator+(LhsT lhs, RhsT rhs) noexcept { return math::detail::apply_arithmetic_op(lhs, rhs, std::plus<>{}); }

template <typename LhsT, typename RhsT, typename std::enable_if_t<math::detail::any_vector<LhsT, RhsT>>* = nullptr>
auto constexpr operator-(LhsT lhs, RhsT rhs) noexcept { return math::detail::apply_arithmetic_op(lhs, rhs, std::minus<>{}); }

template <typename LhsT, typename RhsT, typename std::enable_if_t<math::detail::any_vector<LhsT, RhsT>>* = nullptr>
auto constexpr operator*(LhsT lhs, RhsT rhs) noexcept { return math::detail::apply_arithmetic_op(lhs, rhs, std::multiplies<>{}); }

template <typename LhsT, typename RhsT, typename std::enable_if_t<math::detail::any_vector<LhsT, RhsT>>* = nullptr>
auto constexpr operator/(LhsT lhs, RhsT rhs) noexcept { return math::detail::apply_arithmetic_op(lhs, rhs, std::divides<>{}); }

template <typename LhsT, typename RhsT, typename std::enable_if_t<math::detail::any_vector<LhsT, RhsT>>* = nullptr>
decltype(auto) operator+=(LhsT lhs, RhsT rhs) noexcept { return math::detail::apply_arithmetic_op(lhs, rhs, std::plus<>{}); }

template <typename LhsT, typename RhsT, typename std::enable_if_t<math::detail::any_vector<LhsT, RhsT>>* = nullptr>
decltype(auto) operator-=(LhsT lhs, RhsT rhs) noexcept { return math::detail::apply_arithmetic_op(lhs, rhs, std::minus<>{}); }

template <typename LhsT, typename RhsT, typename std::enable_if_t<math::detail::any_vector<LhsT, RhsT>>* = nullptr>
decltype(auto) operator*=(LhsT lhs, RhsT rhs) noexcept { return math::detail::apply_arithmetic_op(lhs, rhs, std::multiplies<>{}); }

template <typename LhsT, typename RhsT, typename std::enable_if_t<math::detail::any_vector<LhsT, RhsT>>* = nullptr>
decltype(auto) operator/=(LhsT lhs, RhsT rhs) noexcept { return math::detail::apply_arithmetic_op(lhs, rhs, std::divides<>{}); }

template <typename T, std::size_t Size, typename DerivedLhs, typename DerivedRhs>
bool constexpr operator==(math::vector<T, Size, DerivedLhs> const &lhs, math::vector<T, Size, DerivedRhs> const &rhs) noexcept {
  return lhs.data == other.data;
}

template <typename T, std::size_t Size, typename DerivedLhs, typename DerivedRhs>
bool constexpr operator!=(math::vector<T, Size, DerivedLhs> const &lhs, math::vector<T, Size, DerivedRhs> const &rhs) noexcept {
  return !operator==(lhs, rhs);
}