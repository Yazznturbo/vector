#pragma once

#include "vector3.hpp"

namespace math {

template <typename T>
struct vector_base<T, 4>
{
  union
  { std::array<T, 4> data; // active member

    struct { T x, y, z, w; };
    struct { vector2<T> xy, zw; };
    vector3<T> xyz;
  };
};

template <typename T>
class vector4
  : public vector<T, 4, vector4<T>>
{
public:
  using base = vector<T, 4, vector4<T>>;
  using base::base;

  constexpr vector4() noexcept = default;
	constexpr vector4(T x, T y, T z, T w) noexcept 
    : base { x, y, z, w }
  { }

  explicit constexpr vector4(vector2<T> const &xy, T z = T(0), T w = T(0)) noexcept
    : base{ xy[0], xy[1], z, w }
  { }

  explicit constexpr vector4(vector2<T> const &xy, vector2<T> const &zw) noexcept
    : base{ xy[0], xy[1], zw[0], zw[1] }
  { }

  explicit constexpr vector4(vector3<T> const &xyz, T w) noexcept
    : base{ xyz[0], xyz[1], xyz[2], w }
  { }
};

using vec4i = vector4<int>;
using vec4f = vector4<float>;
using vec4d = vector4<double>;

} // namespace math