#pragma once

#include "../vector.hpp"

namespace math {

template <typename T>
struct vector_base<T, 2>
{
  union
  { std::array<T, 2> data; // active member

    struct { T x, y; };
  };
};

template <typename T>
class vector2
  : public vector<T, 2, vector2<T>>
{
public:
  using base = vector<T, 2, vector2<T>>;
  using base::base;

  constexpr vector2() noexcept = default;
	constexpr vector2(T x, T y) noexcept
    : base{ x, y }
  { }
};

using vec2i = vector2<int>;
using vec2f = vector2<float>;
using vec2d = vector2<double>;

} // namespace math