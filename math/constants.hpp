﻿#pragma once

#include <limits>

namespace math
{

/* Euler's number constant */
template <typename T> static constexpr T e = T(2.71828182845904523536L);
/* log2(e) constant */
template <typename T> static constexpr T log2e = T(1.44269504088896340736L);
/* log10(e) constant*/
template <typename T> static constexpr T log10e = T(0.434294481903251827651L);
/* ln(2) constant*/
template <typename T> static constexpr T ln2 = T(0.693147180559945309417L);
/* ln(10) constant*/
template <typename T> static constexpr T ln10 = T(2.30258509299404568402L);
/* π constant*/
template <typename T> static constexpr T pi = T(3.14159265358979323846L);
/* π/2 constant*/
template <typename T> static constexpr T pi_2 = T(1.57079632679489661923L);
/* π/4 constant*/
template <typename T> static constexpr T pi_4 = T(0.785398163397448309616L);
/* sqrt(2)constant*/
template <typename T> static constexpr T sqrt2 = T(1.41421356237309504880L);
/* 1/π constant*/
template <typename T> static constexpr T one_div_pi = T(0.318309886183790671538L);
/* 2/π constant*/
template <typename T> static constexpr T two_div_pi = T(0.636619772367581343076L);
/* 2/sqrt(π)constant*/
template <typename T> static constexpr T two_div_sqrtpi = T(1.12837916709551257390L);
/* 1/sqrt(2) constant */
template <typename T> static constexpr T one_div_sqrt2 = T(0.707106781186547524401L);
/* nan constant */
template <typename T> static constexpr T nan = std::numeric_limits<T>::quiet_NaN();
/* inf constant */
template <typename T> static constexpr T inf = std::numeric_limits<T>::infinity();

template <typename T>
constexpr inline T rad2deg(T rad) {
  return rad * T(180) / pi<T>;
};

template <typename T>
constexpr inline T deg2rad(T deg) {
  return deg * pi<T> / T(180);
};

} // namespace math
