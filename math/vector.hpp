#pragma once

#include <array>
#include <numeric>
#include <functional>

#include "detail/algorithm.hpp"

namespace math {

template <typename T, std::size_t Size>
struct vector_base { std::array<T, Size> data; };

template <typename T, std::size_t Size, typename Derived = void>
class vector
  : public vector_base<T, Size> {
public:
  using base = vector_base<T, Size>;
  using base::data; 
  using vector_tag_type = void;
  using derived_type = std::conditional_t<std::is_same_v<Derived, void>, vector, Derived>;
  using value_type = T;

  static auto constexpr size = Size;

  constexpr vector() noexcept
    : base{}
  { }

	template <typename ...U, typename std::enable_if_t<sizeof...(U) + 1 == Size, T>* = nullptr>
	constexpr vector(T first, U... next) noexcept
    : base{{ first, next... }}
  { }

  explicit constexpr vector(T const scalar) noexcept
    : base{ array_filled(scalar, std::make_index_sequence<Size>{}) }
  { }

  constexpr vector(T const *data) noexcept
    : base{ array_from_data(data, std::make_index_sequence<Size>{}) }
  { }

	constexpr static auto &from(T *data) {
		return *reinterpret_cast<decltype(get_derived()) *>(data);
	}

	constexpr static const auto &from(T const *data) {
		return *reinterpret_cast<decltype(get_derived()) const *>(data);
	}

  explicit constexpr operator T *() {
		return std::data(data);
	}

  explicit constexpr operator T const *() const {
		return std::data(data);
	}

	T constexpr &operator[](std::size_t pos) {
		return data[pos];
	}
  
	T constexpr const &operator[](std::size_t pos) const {
		return data[pos];
	}

	T constexpr &at(std::size_t pos) { //cexpr
		return data.at(pos);
	}

	T constexpr const &at(std::size_t pos) const { //cexpr, const
		return data.at(pos);
	}

	template<typename U = T, typename std::enable_if_t<std::is_integral_v<U>>* = nullptr>
  bool constexpr is_zero() const noexcept { //cexpr, const, noexcept, enable_if
    return vector{} == *this;
	}

  template<typename U = T, typename std::enable_if_t<!std::is_integral_v<U>>* = nullptr>
  bool is_zero() const noexcept {
    //const, enable_if
		//TODO: check constexpr, noexcept
    return std::abs(dot()) < std::numeric_limits<T>::epsilon();
	}

  bool constexpr is_normalized() const noexcept {
    //const, enable_if
    //TODO: check constexpr, noexcept
    return std::abs(dot() - T(1)) < std::numeric_limits<T>::epsilon() * T(2);
	}

  template <typename Derived>
  T constexpr dot(vector<T, Size, Derived> const &other) const
  { return math::dot(*this, other); }

  T constexpr dot() const
  { return math::dot(*this, *this); }

  T constexpr sum() const
  { return math::sum(*this); }

  T constexpr (min)() const
  { return (math::min)(*this); }

  T constexpr (max)() const
  { return (math::max)(*this); }

  T constexpr product() const
  { return math::product(*this); }

  T constexpr length_squared() const
  { return dot(); }

  T length() const
  { return std::sqrt(dot()); }

	T reciprocal_length() const
  { return T(1) / length(); }

  template <typename Derived>
  T distance(vector<T, Size, Derived> const &other) const
  { return (*this - other).length(); }

  derived_type resized(T scale) const
  { return get_derived() * (reciprocal_length() * scale); }

  derived_type normalized() const
  { return get_derived() * reciprocal_length();  }

  void resize(T scale)
  { *this *= (reciprocal_length() * scale); }

  void normalize()
  { *this *= reciprocal_length(); }
	

  // TODO: template for Derived parameter
  friend std::ostream &operator<<(std::ostream &os, vector const &v) {
		os << '(';
		for (std::size_t i = 0; i < Size; ++i)
			os << v[i] << ((i != Size - 1) ? ", " : ")");

		return os;
	}
protected:

  derived_type constexpr &get_derived() {
    return static_cast<derived_type &>(*this);
  }

  derived_type constexpr const &get_derived() const {
    return static_cast<derived_type const &>(*this);
  }

  // TODO: Replace with std::experimental::to_array at some point
  template <std::size_t ...Is>
  constexpr auto array_from_data(T const *data, std::index_sequence<Is...>) const noexcept {
    return std::array<T, sizeof...(Is)>{ data[Is]... };
  }

  // TODO: Replace with std::fill/(_n) in C++20?
  template <std::size_t ...Is>
  constexpr auto array_filled(T const scalar, std::index_sequence<Is...>) const noexcept {
    return std::array<T, sizeof...(Is)>{ (Is, scalar)... };
  }

	static_assert(std::is_arithmetic_v<T>, "type T must be arithmetic");
  static_assert(Size > 1,"size must be >1");
};

template <typename T, std::size_t Size, typename Derived1, typename Derived2>
constexpr T dot(vector<T, Size, Derived1> const &v1, vector<T, Size, Derived2> const &v2) noexcept {
  return detail::inner_product(std::begin(v1.data), std::end(v1.data), std::begin(v2.data), T(0));
}

template <typename T, std::size_t Size, typename Derived>
constexpr T sum(vector<T, Size, Derived> const &v) noexcept {
  return detail::accumulate(std::begin(v.data), std::end(v.data), T(0), std::plus<T>{});
}

template <typename T, std::size_t Size, typename Derived>
constexpr T product(vector<T, Size, Derived> const &v) noexcept {
  return detail::accumulate(std::begin(v.data), std::end(v.data), T(1), std::multiplies<T>{});
}

template <typename T, std::size_t Size, typename Derived>
constexpr T (min)(vector<T, Size, Derived> const &v) noexcept {
	return *std::min_element(std::begin(v.data), std::end(v.data));
}

template <typename T, std::size_t Size, typename Derived>
constexpr T (max)(vector<T, Size, Derived> const &v) noexcept {
	return *std::max_element(std::begin(v.data), std::end(v.data));
}

} // namespace math

#include "detail/vector2.hpp"
#include "detail/vector3.hpp"
#include "detail/vector4.hpp"
#include "detail/vector_ops.hpp"